terraform {
  required_providers {
    google = {
      version = "~> 4.0.0"
    }
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = "3.16"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
  }
}

provider "google" {
  credentials = file("${path.module}/../.never/prod-svc-creds.json")
  project     = var.google_project
  region      = var.google_region
  zone        = var.google_zone
}

provider "gitlab" {
  token = var.gitlab_token
}

provider "helm" {
  kubernetes {
    host                   = "https://${google_container_cluster.primary.endpoint}"
    cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
    token                  = data.google_client_config.current.access_token
  }
}

provider "kubernetes" {
  host                   = "https://${google_container_cluster.primary.endpoint}"
  cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
  token                  = data.google_client_config.current.access_token
}