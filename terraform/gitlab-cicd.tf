
resource "gitlab_group_variable" "kube-ingress-base-domain" {
  group             = var.gitlab_agent_group
  key               = "KUBE_INGRESS_BASE_DOMAIN"
  value             = "${data.kubernetes_service.ingress_controller.status.0.load_balancer.0.ingress.0.ip}.sslip.io"
  protected         = false
  masked            = false
  environment_scope = "*"
}


resource "gitlab_group_variable" "kube-namespace" {
  group             = var.gitlab_agent_group
  key               = "KUBE_NAMESPACE"
  value             = "$${CI_PROJECT_ID}-$${CI_ENVIRONMENT_SLUG}"
  protected         = false
  masked            = false
  environment_scope = "*"
}

resource "gitlab_group_variable" "kube-context" {
  group             = var.gitlab_agent_group
  key               = "KUBE_CONTEXT"
  value             = "${data.gitlab_project.demo-foundation.path_with_namespace}:${helm_release.gitlab_agent.name}"
  protected         = false
  masked            = false
  environment_scope = "*"
}
