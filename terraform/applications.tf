resource "kubernetes_namespace" "gitlab-managed-apps" {
  metadata {
    name = "gitlab-managed-apps"
  }
}

resource "helm_release" "ingress" {

  depends_on = [
    google_container_cluster.primary
  ]

  name = "nginx"

  repository   = "https://kubernetes.github.io/ingress-nginx"
  chart        = "ingress-nginx"
  namespace    = "gitlab-managed-apps"
  version      = "4.2.0"
  force_update = true

  cleanup_on_fail = true

}


data "kubernetes_service" "ingress_controller" {
  depends_on = [
    helm_release.ingress
  ]
  metadata {
    name      = "nginx-ingress-nginx-controller"
    namespace = "gitlab-managed-apps"
  }
}

resource "helm_release" "issuer" {
  depends_on = [helm_release.cert-manager]

  name      = "certs"
  namespace = "gitlab-managed-apps"
  chart     = "${path.module}/cm/issuer-chart"

  force_update    = true
  cleanup_on_fail = true
  recreate_pods   = false
  reset_values    = false

  create_namespace = true

  values = [
    file("./cm/issuer-chart/values.yaml")
  ]
}

resource "helm_release" "cert-manager" {
  depends_on = [
    helm_release.cert-crds
  ]
  name       = "cert-manager"
  namespace  = "gitlab-managed-apps"
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"

  force_update     = true
  create_namespace = true

  # set {
  #   name  = "installCRDs"
  #   value = true
  # }
  values = [
    file("values.yaml")
  ]

}

resource "helm_release" "cert-crds" {
  name      = "certmanager-crds"
  namespace = "gitlab-managed-apps"
  chart     = "${path.module}/cm/crds-chart"

}