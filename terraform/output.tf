# output "ip" {
#   value = google_compute_instance.this.network_interface.0.access_config.0.nat_ip
# }
output "env-dynamic-url" {
  value = "https://${google_container_cluster.primary.endpoint}"
}

output "primary" {
  value = google_container_cluster.primary.endpoint
}

output "primarydebug" {
  value = google_container_cluster.primary.endpoint
}
output "public-ip" {
  value = data.kubernetes_service.ingress_controller.status.0.load_balancer.0.ingress.0.ip
}