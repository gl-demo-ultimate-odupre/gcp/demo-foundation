# Demo Foundation

## Table of Contents

- [Demo Foundation](#demo-foundation)
  - [Table of Contents](#table-of-contents)
  - [Goal](#goal)
  - [Setup aka prerequisites](#setup-aka-prerequisites)
    - [Setup Google Credentials](#setup-google-credentials)
    - [Add CI/CD variables](#add-cicd-variables)
    - [Run the Pipeline to build your GKE cluster](#run-the-pipeline-to-build-your-gke-cluster)
    - [Cleanup](#cleanup)

Pre-work after this project is forked is to set create a service account in your desired gcp project and then set a GOOGLE_JSON group variable with the json of that service account. Also need to create an access token to the group and call it TF_VAR_gitlab_token.

## Goal

Get started ASAP with your own demo environment by:

1. Creating your own, brand new, cluster
2. Adding an agent config
3. Installing that agent into your cluster
4. Deploy all the necessary Infrastructure to allow Auto-DevOps to work, and more...

## Setup aka prerequisites

### Setup Google Credentials

Create your own GCP Project using [https://gitlabsandbox.cloud](https://gitlabsandbox.cloud) by following the [instructions in the 📙 handbook](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/#individual-aws-account-or-gcp-project)

Now we'll need to prepare a service account, in order to populate the `GOOGLE_CREDENTIALS`-variable later.
The following commands can be run either in your local terminal (after [installing](https://cloud.google.com/sdk/docs/install) and setting up the gcloud cli-tool - don't forget to [authorize](https://cloud.google.com/sdk/docs/authorizing) before running any commands), or in the [Cloud Shell](https://cloud.google.com/shell/docs/using-cloud-shell) in your [Google Cloud Console](https://console.cloud.google.com/). In either case, make sure you are working in the correct `GOOGLE_PROJECT`.
Go to your projects:
![Find projects](images/find_projects.png)
And select the right one:

![Select the right GCP Project](images/gcp_personal_project.png)

1. Make sure you have the config set to your project and the `Compute` and `Kubernetes` APIs enabled in your Google Account.

   ```shell
   gcloud config set project <GOOGLE_PROJECT>
   ```

   ```shell
    gcloud services enable compute.googleapis.com container.googleapis.com
   ```

2. Save the `GOOGLE_PROJECT` value in an environment variable for later use by replacing `<GOOGLE_PROJECT>` with your project name

   ```shell
    export GOOGLE_PROJECT="<GOOGLE_PROJECT>"
   ```

3. Create a new service account. e.g. `gitops4k` by running following command: (make sure to save the output, as you'll need it to fill in the `<SERVICE_ACCOUNT>` in the next step)

    ```shell
     gcloud iam service-accounts create gitops4k
    ```

4. We will need to validate the account was created, and have the `email`-format of the service-account to use later. You can get to it by generating the list of service-accounts, using this command:

   ```shell
    gcloud iam service-accounts list
   ```

    Find the newly created `service-account` in the list, and save the email-format in an environment variable by running the command below, after replacing the `<SERVICE_ACCOUNT>` with the `gitops4k` `EMAIL` found in the last command

   ```shell
    export SERVICE_ACCOUNT="<SERVICE_ACCOUNT>"
   ```

5. Next, we need to make sure the service account has sufficient rights. Run the following command, after replacing the `<GOOGLE_PROJECT>` and `<SERVICE_ACCOUNT>`-placeholders with the appropriate values by exporting them into Environment Variables

    ```shell
    gcloud projects add-iam-policy-binding $GOOGLE_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT" --role=roles/compute.instanceAdmin 
    gcloud projects add-iam-policy-binding $GOOGLE_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT" --role=roles/container.clusterAdmin 
    gcloud projects add-iam-policy-binding $GOOGLE_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT" --role=roles/container.admin 
    gcloud projects add-iam-policy-binding $GOOGLE_PROJECT --member="serviceAccount:$SERVICE_ACCOUNT" --role=roles/iam.serviceAccountUser 
    ```

    (#TODO): Reduce the permissions, we currently provide too much:
    - **Compute Admin:** `roles/compute.instanceAdmin`
    - **Kubernetes Engine Admin:** `roles/container.clusterAdmin` # Needed to create the cluster
    - **Kubernetes Engine Admin:** `roles/container.admin`  # Needed to delete resources in case of failure of the Helm chart or when deleting the cluster.
    - **Service Account User:** `roles/iam.serviceAccountUser`

6. Now that the service account was created, and the permissions where assigned, we need to generate a json-file which we can use to populate the `GOOGLE_CREDENTIALS`-variable below.

   ```shell
   gcloud iam service-accounts keys create google_credentials.json --iam-account=$SERVICE_ACCOUNT
   ```

   **Troubleshooting Tip:** Verify that you have selected the right service account (should start with 'gitops4k') by reviewing the google_credentials.json file that you will load into GitLab CI/CD variables. Also, navigate to Google Cloud's IAM > Admin page to verify that the gitops4k serivce account that you are using has the appropriate roles assigned: Compute Admin, Kubernetes Engine Admin, and Service Account User.

### Add CI/CD variables

Now that we have generated our credentials file, we need to add it as a CI/CD variable in either the project or in the group hiearchy where this project belongs to.

Next to this we also need a GitLab **Personal Access Token** with the `api` scope in order to create the agent and add the CI/CD variables in the Gitlab group, as part of the Terraform script.

So the following needs to be set up:

| Key                   | Value                                                                                                                                  | Protected | Masked |
| --------------------- | -------------------------------------------------------------------------------------------------------------------------------------- | --------- | ------ |
| 🔐 GOOGLE_CREDENTIALS  | Contents of the `google_credentials.json`-file generated as part of [setting up the google credentials](#setup-google-credentials) above | ❌         | ❌      |
| 🔐 TF_VAR_gitlab_token | `<Generate A Personal Access Token>` via [Access Tokens](/-/profile/personal_access_tokens)                                           | ❌         | ✅      |
| 🔐 TF_VAR_google_project | Value of the GOOGLE_PROJECT variable defined above.                                                                                 | ❌         | ❌      |
| 🔐 TF_VAR_google_region  | `<optional>` ID of the Google region where the deployment will be made. Defaults to `"us-central1"`                                 | ❌         | ❌      |
| 🔐 TF_VAR_google_zone    | `<optional>` ID of the Google zone where the deployment will be made. Defaults to `"us-central1-a"`                                 | ❌         | ❌      |

### Run the Pipeline to build your GKE cluster

Run the pipeline for the `main`-branch to start the creation process.
This will:

1. Create a GKE cluster with `AutoPilot` enabled
2. Create an agent config in the current project, generate a `token` for it and register it
3. Run the helm installation of the agent, connecting it to gitlab, and making it available to the group specified in `ci_access:`-portion of the agent config (Defaults to the current parent group of this project)
4. Installs the necessary K8S components: ingress-nginx ([**NOT** nginx-ingress!](https://grigorkh.medium.com/there-are-two-nginx-ingress-controllers-for-k8s-what-44c7b548e678), which we found out the hard way...) and cert-manager
5. Sets up the necessary CI/CD Variables for AutoDevOps to work in all projects and sub-groups belonging to the partent group of this project:
   - `KUBE_INGRESS_BASE_DOMAIN`: set to `(ip address of the LoadBalancer).sslip.io`
   - `KUBE_NAMESPACE`: set to `$${CI_PROJECT_ID}-$${CI_ENVIRONMENT_SLUG}`
   - `KUBE_CONTEXT`: set to the current project and gitlab agent : `<project_with_path>:<agent-name>`

### Cleanup

- Run the `destroy` job in the pipeline to remove the cluster and the agent configuration
- Delete the following `CI/CD Variables`: `GOOGLE_CREDENTIALS` and `TF_VAR_gitlab_token`
